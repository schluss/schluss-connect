'use strict';

// Load modules ---------------------------------------

var browserify = require('browserify');

var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var log = require('gulplog');
var webserver = require('gulp-webserver');
var clean = require('gulp-clean');
var runsequence = require('run-sequence');
var css2js = require("gulp-css2js");
var cssmin = require("gulp-cssmin");
var jshint = require('gulp-jshint');

// Configuration settings ---------------------------------------

var config = {
	
	base : { 
		src : './src/',
		dist : './dist/'
	},
	
	serverport : 8080
};

// Tasks ---------------------------------------

// code hinting
gulp.task( 'hint-js', function() {
  return gulp.src('src/scripts/schluss-connect.js')
    .pipe( jshint() )
    .pipe( jshint.reporter('default') );
});

// Clean: empty release folder, delete tmp files
gulp.task('clean', function() {
    return gulp.src(config.base.dist, {read: false})
        .pipe(clean()); // del dist folder
});

// Copy: copy needed folder/files to release folder
gulp.task('copy', function() {
	// copy index.html to dist
	return gulp.src(config.base.src + "index.html") 
	.pipe(gulp.dest(config.base.dist));	
});

// Javascript: Bundle and minify JS files
gulp.task('javascript', function () {
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: config.base.src + 'scripts/schluss-connect.js',
    debug: false
  });

  return b.bundle()
    .pipe(source('schluss-connect.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
        // Add transformation tasks to the pipeline here.
        .pipe(uglify())
        .on('error', log.error)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.base.dist));
});

// CSS: Minify CSS files, convert them to JS for bundling
gulp.task('css', function () {
	return gulp.src(config.base.src + 'assets/css/style.css')
	.pipe(cssmin()) // minify css
	// convert it to JavaScript and specify options
	.pipe(css2js({
		splitOnNewline: false
	}))
	// write to output
	.pipe(gulp.dest(config.base.src + 'scripts/'));
});

// Webserver: run local dev webserver
gulp.task('webserver', function() {
  gulp.src(config.base.dist)
  //gulp.src('./dist')
    .pipe(webserver({
		host 			: 'localhost',
		port 			: config.serverport,
		https 			: true, 
		livereload		: true,
		directoryListing: true,
		open			: 'index.html',
    }));
});

// Build: copy and bundle needed files
gulp.task('build', gulp.series('clean', gulp.parallel('copy', 'javascript', 'css')));

// Default: build and start local development webserver
gulp.task('default', gulp.series('build', 'webserver'));