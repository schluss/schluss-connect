# ![Schluss](schluss-logo.svg) Connect

An embeddable javascript library that makes it possible for webapps to connect to the Schluss ecosystem. Its designed to be as easy as possible to integrate this inside any webbased application.

![](qr-example.png)   

## How it works

When integrated in your webapp your user will see a QR code. When the user scans the code with their (mobile) device or clicks on it he or she will be forwarded to the Schluss App (https://schluss.app). 
In here the connection process starts. What data can be connected with you app is defined in your config.json file. Read more about config.json files in https://gitlab.com/schluss/schluss-serviceprovider. When the user does not have a Schluss vault on the device yet, first the Schluss onboarding starts by helping the user to create a Schluss vault.

## How to integrate
The following steps will guide you to make the integration in your webpage as smooth as possible.

### 1. Config.json
All Schluss connect related configuration settings are defined in one config.json file. This file must be hosted on a publically available location, for example: https://your-site.com/config.json. Documentation on how to create a valid config.json file is found in https://gitlab.com/schluss/schluss-serviceprovider. 

#### 2. Define a container
The QR code has to be visible somewhere on the page. To do so, make sure you add an element like this:

```sh
<div id="qrcontainer"></div>
```

#### 3. Embed javascript code in your page
To integrate the QR code javascript, add the following just before the closing body tag on your page:


##### Automatically and show directly after loading of the page
```sh
	<script type="text/javascript">
	  var _schlusscfg = _schlusscfg || {
		canvas 		: 'qrcontainer',						// id of container element to display the QR code
		configUrl 	: 'https://path.to/config.json',	    // FULL url to your organisations config.json file
		token 		: '[your-unique-generated-token]',		// token to identify this session later on
		};
	
	  (function() {
		var u="schluss-connect.js"; // path to the schluss-connect.js file
		var d=document, c=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		c.type='text/javascript'; c.async=false; c.defer=true; c.src=u; s.parentNode.insertBefore(c,s);
	  })();
	</script>
```

##### Manually initialize and generate the QR code
```sh
	<script type="text/javascript">

	  (function() {
		var u="schluss-connect.js"; // path to the schluss-connect.js file
		var d=document, c=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		c.type='text/javascript'; c.async=false; c.defer=true; c.src=u; s.parentNode.insertBefore(c,s);
	  })();
	  
	  // make sure the onload event has fired
	  document.addEventListener("DOMContentLoaded", function(event) {

		// run your code here
		// ....
		
		// ..and when you are ready to do so: declare the settings
		var _schlusscfg = _schlusscfg || {
		 canvas 		: 'qrcontainer',						// id of container element to display the QR code
		 configUrl 	: 'https://path.to/config.json',	    // FULL url to your organisations config.json file
		 token 		: '[your-unique-generated-token]',		// token to identify this session later on
		};		

		// init and run the QR code generator
		SchlussConnect.init(_schlusscfg);

	  });	  
	</script>
```

About the configuration parameters:

- canvas [required]: must the id of the html element that will display the qr code
- configurl [required]: contains the full url to your config.json file.
- token [required]: an unique identifier that the Schluss app will use to report back to you about the progress of the connecting process.

Optional parameters:

- size: specify the size (in pixels) of the QR code, default: 200
- clickable: specify whether the QR code is clickable or not, default: false
- rounded: specify the amound of roundness in the edges of the blocks inside the QR code, default: 50 (choose between 0 and 100)
- debug: enable/disable debugging and verbose messages, default: false


## For developers
Clone this repository and get started by following the instructions below 

Via NPM, run the following command:
> npm install

### Run the code
To build the code and launch a local webserver, run: 
> npm sun serve

### Building
To build the code, run:
> npm run build

### JS Code hinting
To validate your code, run:
> npm run check

### Run local webserver only
To start the local development webserver, run:
> npm run webserver

## Structure
[to come]

