/**
 * Schluss Connect
 * Create a QR code to Connect to the Schluss ecosystem using this library
 *
 * @version 0.0.7
 * @author Bob Hageman (bob@schluss.org), Schluss
 * @see <a href="https://www.schluss.org" target="_blank">www.schluss.org</a> 
 * @see <a href="https://gitlab.com/schluss/schluss-connect" target="_blank">https://gitlab.com/schluss/schluss-connect</a> 
 */

var css = require('./style.js'); // embed css

// Make SchlussConnect globally available
window.SchlussConnect = {
	
	// default config settings
	cfg : {
		canvas 			: '',
		size 			: 200, 		// width / height in pixels
		clickable		: false, 	// is the QR code clickable?
		redirectUrl 	: 'https://schluss.org/redirect/connect/[token]/[configurl]/[scope]',
		scope			: '',
		rounded			: 50,
		debug			: false
	},
	
	init : function(cfg){
				
		// merge default cfg with customised cfg
		if (cfg) {
			for (var i in cfg) {
				this.cfg[i] = cfg[i];
			}
		}
		
		try {
			// sanity checks
			if (this.cfg.canvas === '' || document.getElementById(this.cfg.canvas) === undefined)
				throw 'no qr canvas element id invalid';
			
			if (this.cfg.configUrl === undefined)
				throw 'no config.json url defined';			
			
			if (this.cfg.token == undefined)
				throw 'no token defined';
			
			// make the url
			var url = this.cfg.redirectUrl.replace('[token]', encodeURIComponent(this.cfg.token));
			url = url.replace('[configurl]', encodeURIComponent(this.cfg.configUrl));
			url = url.replace('[scope]', encodeURIComponent(this.cfg.scope || ''));
			
			if (this.cfg.debug)
				console.log('Schluss Connect - QR code url: ' + url);
			
			// create container
			var container = document.createElement('div');
			container.className = 'schluss-qr-container';
			
			// make clickable, if enabled
			if (this.cfg.clickable) {
				if (container.addEventListener) {  // all browsers except IE before version 9
				  container.addEventListener('click', function(){schlussconnect.clickQr(url);}, false);
				} 
				else if (container.attachEvent) {   // IE before version 9
					container.attachEvent('click', function(){schlussconnect.clickQr(url);});
				}	
			}	
			
			// create QR
			var el = require('kjua')({
					render: 'image',
					text: url,
					rounded: this.cfg.rounded, // 0 to 100
					ecLevel: 'L', // L, M, Q, H
					fill: '#000000', // foreground color
					back: '#ffffff', // background color
					crisp: true,
					size: this.cfg.size // in pixels
			});
		
			// add QR to container
			container.appendChild(el);

			// append to canvas element
			document.getElementById(cfg.canvas).appendChild(container);
		}
		catch(e) {
			console.log('Schluss Connect - Init failed: ');
			console.log(e);
		}
	},
	
	clickQr : function(url) {
		window.open(url);
	}
};

// When the config variables are during load time in place already, we assume the script is to be loaded and executed directly after loading
if (typeof _schlusscfg !== 'undefined') {
	document.addEventListener('DOMContentLoaded', function(event) {
	
		window.SchlussConnect.init(_schlusscfg);
	
	});
}

module.exports = window.SchlussConnect;